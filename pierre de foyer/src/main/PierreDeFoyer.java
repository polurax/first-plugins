package main;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class PierreDeFoyer extends ItemStack {
    private static final String PIERRE_DE_FOYER = "Pierre de Foyer";
    private static final String INIT_LOCATION = "-";
    private static final String SEPARATOR_LOCATION = " ";
    private static final String LORE_1 = "sneak + clic droit -> enregistrement position";
    private static final String LORE_2 = "clic droit -> teleportation";

    public PierreDeFoyer() {
        super(Material.STICK);
        ItemMeta meta = this.getItemMeta();
        meta.setDisplayName(PIERRE_DE_FOYER);
        meta.setLore(Arrays.asList(INIT_LOCATION, LORE_1, LORE_2));
        this.setItemMeta(meta);
    }

    public static void setLocation(ItemStack item, Location location) {
        String strLocation = location.getBlockX() +
                SEPARATOR_LOCATION + location.getBlockY() +
                SEPARATOR_LOCATION + location.getBlockZ() +
                SEPARATOR_LOCATION + location.getYaw() +
                SEPARATOR_LOCATION + location.getPitch();
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName("Pierre de Foyer");
        meta.setLore(Arrays.asList(strLocation, LORE_1, LORE_2));
        item.setItemMeta(meta);
    }

    public static Location getLocation(ItemStack item, World world) {
        String[] coordonnees = item.getItemMeta().getLore().get(0).split(SEPARATOR_LOCATION);
        if (coordonnees.length == 5) {
            return new Location(world, Double.valueOf(coordonnees[0]), Double.valueOf(coordonnees[1]), Double.valueOf(coordonnees[2]), Float.valueOf(coordonnees[3]), Float.valueOf(coordonnees[4]));
        }
        return null;
    }

    public static boolean estUnePierreDeFoyer(ItemStack item) {
        return item != null && item.hasItemMeta() &&
                item.getItemMeta().hasDisplayName() &&
                item.getItemMeta().hasLore() &&
                item.getItemMeta().getDisplayName().equals(PIERRE_DE_FOYER) &&
                item.getItemMeta().getLore().get(1).equals(LORE_1) &&
                item.getItemMeta().getLore().get(2).equals(LORE_2);
    }

    public static boolean possedePierreDeFoyer(Player player) {
        ItemStack[] inventory = player.getInventory().getContents();
        int i = 0;
        while (i < inventory.length) {
            if (estUnePierreDeFoyer(inventory[i])) return true;
            i++;
        }
        return false;
    }
}
