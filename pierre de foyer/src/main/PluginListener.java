package main;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;

public class PluginListener implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        event.setJoinMessage("Salut !");
        Player player = event.getPlayer();
        ItemStack itemCustom = new PierreDeFoyer();
        if (!PierreDeFoyer.possedePierreDeFoyer(event.getPlayer())){
            player.getInventory().addItem(itemCustom);
        }
    }
    @EventHandler
    public void onUse(PlayerInteractEvent event) {
        //System.out.println(event.getAction().name() +" "+ event.getItem());
        if (event.getAction().name().startsWith("RIGHT_CLICK") && PierreDeFoyer.estUnePierreDeFoyer(event.getItem())){
            if (event.getPlayer().isSneaking()){
                //System.out.println("register");
                PierreDeFoyer.setLocation(event.getItem(), event.getPlayer().getLocation());
            }else{
                //System.out.println("teleport");
                Location location = PierreDeFoyer.getLocation(event.getItem(), event.getPlayer().getWorld());
                if (location != null)event.getPlayer().teleport(location);
            }
        }
    }
}
