package main;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Iterator;

public class CommandMsgpp implements CommandExecutor {

    private final HashMap<String, ChatColor> MAP_COLORS = new HashMap<>();
    private final HashMap<String, ChatColor> MAP_STYLE = new HashMap<>();
    private final HashMap<String, ChatColor> MAP_LINE = new HashMap<>();
    private final HashMap<Player, PlayerChatSettings> MAP_CHAT_SETTINGS = new HashMap<>();

    public CommandMsgpp() {
        MAP_COLORS.put("bleuc", ChatColor.AQUA);
        MAP_COLORS.put("jaunef", ChatColor.GOLD);
        MAP_COLORS.put("vertc", ChatColor.GREEN);
        MAP_COLORS.put("rouge", ChatColor.RED);
        MAP_COLORS.put("jaune", ChatColor.YELLOW);
        MAP_COLORS.put("blanc", ChatColor.WHITE);
        MAP_COLORS.put("magenta", ChatColor.LIGHT_PURPLE);
        MAP_COLORS.put("grisc", ChatColor.GRAY);
        MAP_COLORS.put("rougef", ChatColor.DARK_RED);
        MAP_COLORS.put("violet", ChatColor.DARK_PURPLE);
        MAP_COLORS.put("vert", ChatColor.DARK_GREEN);
        MAP_COLORS.put("gris", ChatColor.DARK_GRAY);
        MAP_COLORS.put("bleuf", ChatColor.DARK_BLUE);
        MAP_COLORS.put("cyan", ChatColor.DARK_AQUA);
        MAP_COLORS.put("bleu", ChatColor.BLUE);
        MAP_COLORS.put("noir", ChatColor.BLACK);
        MAP_COLORS.put("normal", null);

        MAP_LINE.put("_", ChatColor.UNDERLINE);
        MAP_LINE.put("-", ChatColor.STRIKETHROUGH);
        MAP_LINE.put("normal", null);

        MAP_STYLE.put("italique", ChatColor.ITALIC);
        MAP_STYLE.put("gras", ChatColor.BOLD);
        MAP_STYLE.put("magique", ChatColor.MAGIC);
        MAP_STYLE.put("normal", null);
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        toLowerCase(strings);

        if (commandSender instanceof Player) {
            Player player = (Player) commandSender;
            if (MAP_CHAT_SETTINGS.get(player) == null) MAP_CHAT_SETTINGS.put(player, new PlayerChatSettings(player));

            if (strings.length == 0) {
                player.sendMessage(getTuto());
            } else if (strings[0].equals("style")) {
                execute(strings, MAP_STYLE, player);
            } else if (strings[0].equals("couleur")) {
                execute(strings, MAP_COLORS, player);
            } else if (strings[0].equals("ligne")) {
                execute(strings, MAP_LINE, player);
            } else if (strings[0].equals("efface")) {
                MAP_CHAT_SETTINGS.get(player).efface();
            } else if (strings[0].equals("info")) {
                player.sendMessage(MAP_CHAT_SETTINGS.get(player).toString());
            } else {
                player.chat(MAP_CHAT_SETTINGS.get(player).getPrefix() + getMessage(strings));
            }
            return true;
        }
        return false;
    }

    private void toLowerCase(String[] strings) {
        int i = 0;
        while (i < strings.length){
            strings[i] = strings[i].toLowerCase();
            i++;
        }
    }

    private String getTuto() {
        return getPluginPrefix() +"\n"+
                ChatColor.GOLD+"/msg++ (message): "+ChatColor.RESET +
                "envoi un message avec l'apparence choisie\n" +
                ChatColor.GOLD+"/msg++ couleur | style | ligne (valeur): "+ChatColor.RESET +
                "modifie les paramètres des messages\n" +
                ChatColor.GOLD+"/msg++ info: "+ChatColor.RESET +
                "affiche les paramètres selectionnés\n" +
                ChatColor.GOLD+"/msg++ efface: "+ChatColor.RESET +
                "reinitialise les paramètres";
    }

    private void execute(String[] strings, HashMap<String, ChatColor> map, Player player) {
        if (strings.length > 1 && map.keySet().contains(strings[1])) {
            MAP_CHAT_SETTINGS.get(player).set(strings[0], map.get(strings[1]));
            player.sendMessage(getPluginPrefix() + strings[0] + " = " + strings[1]);
        } else {
            player.sendMessage(getPluginPrefix() + "/msg++ " + strings[0] + " " + getListePossible(map));
        }
    }

    private String getPluginPrefix() {
        return "[" + ChatColor.LIGHT_PURPLE + "message++" + ChatColor.RESET + "] ";
    }

    private String getListePossible(HashMap<String, ChatColor> map) {
        Iterator<String> iterator = map.keySet().iterator();
        StringBuilder str = new StringBuilder();
        while (iterator.hasNext()) {
            str.append(iterator.next());
            if (iterator.hasNext()) str.append(" | ");
        }
        return str.toString();
    }

    private String getMessage(String[] strs) {
        StringBuilder message = new StringBuilder();
        for (String str : strs) {
            message.append(str).append(" ");
        }
        return message.toString();
    }

    private class PlayerChatSettings {
        private Player player;
        private ChatColor style;
        private ChatColor color;
        private ChatColor line;

        public PlayerChatSettings(Player player) {
            this.player = player;
        }

        public void set(String str, ChatColor cc) {
            switch (str) {
                case "style":
                    style = cc;
                    break;
                case "couleur":
                    color = cc;
                    break;
                case "ligne":
                    line = cc;
                    break;
                default:
                    throw new RuntimeException();
            }
        }

        public ChatColor get(String str) {
            switch (str) {
                case "style":
                    return style;
                case "couleur":
                    return color;
                case "ligne":
                    return line;
                default:
                    throw new RuntimeException();
            }
        }

        private String getPrefix() {
            String str = "";
            if (color != null) str += color;
            if (style != null) str += style;
            if (line != null) str += line;
            return str;
        }

        private void efface() {
            color = null;
            style = null;
            line = null;
        }

        @Override
        public String toString() {
            return getPluginPrefix()+"couleur = " + getValue(color) + ", style = " + getValue(style) + ", ligne = " + getValue(line);
        }

        private Object getValue(ChatColor str) {
            return str != null ? str : "normal";
        }
    }
}
