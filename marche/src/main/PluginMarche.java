package main;

import org.bukkit.plugin.java.JavaPlugin;

public class PluginMarche extends JavaPlugin {
    @Override
    public void onEnable() {
        System.out.println("Plugin : Marche -> OK");
        getServer().getPluginManager().registerEvents(new MarcheCustom(), this);
        getCommand("marche").setExecutor(MarcheCustom.getCommandExecutor());
        super.onEnable();
    }
}
