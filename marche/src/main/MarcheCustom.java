package main;

import org.bukkit.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;
import java.util.List;

public class MarcheCustom implements Listener {
    private static final String NAME_MARCHE = ChatColor.RED + "Marché";
    private static final List<String> LORE_MARCHE = Arrays.asList(ChatColor.AQUA + "Ce bloc peut être utilisé", ChatColor.AQUA + "pour acheter certains objects");
    private static final Material MARCHE_MATERIAL = Material.EMERALD_BLOCK;
    public static final String INVENTORY_NAME = "Le marché";

    private enum Articles {

        ETOILE_DU_NETHER(Material.NETHER_STAR, "Etoile du Nether", Arrays.asList("Achetez en une pour 64 diamants"), Material.DIAMOND, 64, 0),
        OEUF_DE_DRAGON(Material.DRAGON_EGG, "Oeuf de Dragon", Arrays.asList("Achetez en un pour 64 diamants"), Material.DIAMOND, 64, 4),
        ELYTRA(Material.ELYTRA, "Elytra", Arrays.asList("Achetez en une paire pour 128 diamants"), Material.DIAMOND, 128, 8);

        private List<String> lore;
        private String name;
        private Material itemCost;
        private int price;
        private Material article;
        private int position;

        Articles(Material article, String name, List<String> lore, Material itemCost, int price, int position) {
            this.article = article;
            this.itemCost = itemCost;
            this.lore = lore;
            this.price = price;
            this.name = ChatColor.GREEN + name;
            this.position = position;
            int i = 0;
            while (i < lore.size()) {
                lore.set(i, ChatColor.GOLD + lore.get(i));
                i++;
            }
        }

        public ItemMeta writeItemMeta(ItemMeta meta) {
            meta.setDisplayName(name);
            meta.setLore(lore);
            return meta;
        }

        public boolean equals(ItemStack item) {
            if (item != null) {
                System.out.println(item != null &&
                        item.hasItemMeta() &&
                        item.getItemMeta().hasDisplayName() &&
                        item.getItemMeta().hasLore());
                System.out.println(item.getItemMeta().getDisplayName().equals(name));
                System.out.println(item.getItemMeta().getLore().equals(lore));
                System.out.println(
                        item.hasItemMeta() &&
                                item.getItemMeta().hasDisplayName() &&
                                item.getItemMeta().hasLore() &&
                                item.getItemMeta().getDisplayName().equals(name) &&
                                item.getItemMeta().getLore().equals(lore));
            }
            return item != null &&
                    item.hasItemMeta() &&
                    item.getItemMeta().hasDisplayName() &&
                    item.getItemMeta().hasLore() &&
                    item.getItemMeta().getDisplayName().equals(name) &&
                    item.getItemMeta().getLore().equals(lore);
        }

        public void achat(Player player) {
            int prix = price;
            int i = 0;
            ItemStack[] items = player.getInventory().getContents();
            while (i < items.length && prix > 0) {
                if (items[i] != null && items[i].getType().equals(itemCost)) {
                    if (items[i].getAmount() <= prix) {
                        prix -= items[i].getAmount();
                        items[i].setAmount(0);
                    } else {
                        items[i].setAmount(items[i].getAmount() - prix);
                        prix = 0;
                    }
                }
                i++;
            }
            player.getInventory().addItem(new ItemStack(article, 1));
        }

        public boolean peutPayer(Player player) {
            int quantite = 0;
            int i = 0;
            ItemStack[] items = player.getInventory().getContents();
            while (i < items.length && quantite < price) {
                if (items[i] != null && items[i].getType().equals(itemCost)) quantite += items[i].getAmount();
                i++;
            }
            return quantite >= price;
        }
    }

    private void openInventory(Player player) {
        int tailleBoutique = 9;
        int i = 0;
        while (i < Articles.values().length) {
            while (tailleBoutique <= Articles.values()[i].position) {
                tailleBoutique += 9;
            }
            i++;
        }
        Inventory inventory = Bukkit.createInventory(player, tailleBoutique, INVENTORY_NAME);
        ItemStack item;
        i = 0;
        while (i < Articles.values().length) {
            item = new ItemStack(Articles.values()[i].article);
            item.setItemMeta(Articles.values()[i].writeItemMeta(item.getItemMeta()));
            inventory.setItem(Articles.values()[i].position, item);
            i++;
        }

        player.openInventory(inventory);
    }

    @EventHandler
    private void onClick(PlayerInteractEvent event) {
        if (event.getAction().name().startsWith("RIGHT") &&
                !event.getPlayer().isSneaking() &&
                event.getClickedBlock().getType() == MARCHE_MATERIAL) {

            openInventory(event.getPlayer());
            event.setCancelled(true);
        }
    }

    @EventHandler
    private void onBuy(InventoryClickEvent event) {
        if (event.getInventory().getName().equals(INVENTORY_NAME)) {
            ItemStack item = event.getCurrentItem();
            if (item != null) System.out.println(item.getItemMeta().getDisplayName());
            Player player = (Player) event.getInventory().getHolder();
            int i = 0;
            Articles[] articles = Articles.values();
            while (i < articles.length && (!articles[i].equals(item) || !articles[i].peutPayer(player))) i++;
            if (i < articles.length) articles[i].achat(player);
            event.setCancelled(true);
        }
    }

    private static ItemStack getMarcheItem() {
        ItemStack item = new ItemStack(MARCHE_MATERIAL);
        ItemMeta itemMeta = item.getItemMeta();
        itemMeta.setDisplayName(NAME_MARCHE);
        itemMeta.setLore(LORE_MARCHE);
        item.setItemMeta(itemMeta);
        return item;
    }

    public static CommandExecutor getCommandExecutor() {
        return new CommandeMarche();
    }

    private static class CommandeMarche implements CommandExecutor {

        @Override
        public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
            if (commandSender instanceof Player) {
                ((Player) commandSender).getInventory().addItem(getMarcheItem());
            }
            return false;
        }
    }
}
