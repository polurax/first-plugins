package main;

import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class PluginListener implements Listener {

    @EventHandler
    public void onBreak(BlockBreakEvent event) {
        Block block = event.getBlock();
        System.out.println(block.getBlockData().getAsString());
        System.out.println(block.getType().name());

        if (block.getType().name().startsWith("LOG")) {
            if (block.getBlockData().getAsString().equals(Tree.SPRUCE.getLogData())) {
                breakTree(Tree.SPRUCE, block);
            } else if (block.getBlockData().getAsString().equals(Tree.OAK.getLogData())) {
                breakTree(Tree.OAK, block);
            } else if (block.getBlockData().getAsString().equals(Tree.BIRCH.getLogData())) {
                breakTree(Tree.BIRCH, block);
            } else if (block.getBlockData().getAsString().equals(Tree.ACACIA.getLogData())) {
                breakTree(Tree.ACACIA, block);
            } else if (block.getBlockData().getAsString().equals(Tree.JUNGLE.getLogData())) {
                breakTree(Tree.JUNGLE, block);
            } else if (block.getBlockData().getAsString().equals(Tree.DARK_OAK.getLogData())) {
                breakTree(Tree.DARK_OAK, block);
            }
        }
    }

    private void breakTree(Tree tree, Block block) {
        System.out.println("1");
        breakLeaves(tree, block, 0);
        System.out.println("2");

        propagationHorizontale(tree, block.getRelative(BlockFace.NORTH));
        propagationHorizontale(tree, block.getRelative(BlockFace.SOUTH));
        propagationHorizontale(tree, block.getRelative(BlockFace.EAST));
        propagationHorizontale(tree, block.getRelative(BlockFace.WEST));
        propagationHorizontale(tree, block.getRelative(BlockFace.NORTH_EAST));
        propagationHorizontale(tree, block.getRelative(BlockFace.SOUTH_WEST));
        propagationHorizontale(tree, block.getRelative(BlockFace.SOUTH_EAST));
        propagationHorizontale(tree, block.getRelative(BlockFace.NORTH_WEST));
        System.out.println("3");

        Block blockDuDessus = block.getRelative(BlockFace.UP);
        if (blockDuDessus.getBlockData().getAsString().equals(tree.getLogData())) {
            breakLog(tree, blockDuDessus);
        } else {
            boolean finDeLarbre;
            System.out.println("----");
            finDeLarbre = propagationDiagonale(tree, blockDuDessus.getRelative(BlockFace.NORTH));
            finDeLarbre = propagationDiagonale(tree, blockDuDessus.getRelative(BlockFace.SOUTH)) && finDeLarbre;
            finDeLarbre = propagationDiagonale(tree, blockDuDessus.getRelative(BlockFace.EAST)) && finDeLarbre;
            finDeLarbre = propagationDiagonale(tree, blockDuDessus.getRelative(BlockFace.WEST)) && finDeLarbre;
            finDeLarbre = propagationDiagonale(tree, blockDuDessus.getRelative(BlockFace.NORTH_WEST)) && finDeLarbre;
            finDeLarbre = propagationDiagonale(tree, blockDuDessus.getRelative(BlockFace.NORTH_EAST)) && finDeLarbre;
            finDeLarbre = propagationDiagonale(tree, blockDuDessus.getRelative(BlockFace.SOUTH_EAST)) && finDeLarbre;
            finDeLarbre = propagationDiagonale(tree, blockDuDessus.getRelative(BlockFace.SOUTH_WEST)) && finDeLarbre;
            System.out.println("fin " + finDeLarbre);
            if (finDeLarbre) {
                breakLeaves(tree, block, 1);
                breakLeaves(tree, block, 2);
            }
        }
    }

    private boolean propagationDiagonale(Tree tree, Block block) {
        //System.out.println(block.getBlockData().getAsString().startsWith(tree.getLogDataWithOutAxis()) + " " + block.getBlockData().getAsString() + " " + tree.getLogDataWithOutAxis());
        if (block.getBlockData().getAsString().startsWith(tree.getLogDataWithOutAxis())) {
            breakLog(tree, block);
            return false;
        }
        return true;
    }

    private void propagationHorizontale(Tree tree, Block block) {
        if (block.getBlockData().getAsString().startsWith(tree.getLogDataWithOutAxis())) {
            breakLog(tree, block);
        }
    }

    private void breakLog(Tree tree, Block block) {
        //System.out.println(block.getBlockData().getAsString() + " " + block.getLocation().getBlockX() + " " + block.getLocation().getBlockY() + " " + block.getLocation().getBlockZ());
        block.breakNaturally();
        breakTree(tree, block);
    }

    private void breakLeaves(Tree tree, Block block, int y) {
        if (y != 0 && block.getRelative(0, y, 0).getBlockData().getAsString().startsWith(tree.getLeavesData()))
            block.getRelative(0, y, 0).breakNaturally();
        breakLeavesPropagation(tree, block.getRelative(0, y, 0), 1);

    }

    private void breakLeavesPropagation(Tree tree, Block block, int distance) {
        breakLeavesAdjacentes(tree, block.getRelative(BlockFace.NORTH), distance);
        breakLeavesAdjacentes(tree, block.getRelative(BlockFace.SOUTH), distance);
        breakLeavesAdjacentes(tree, block.getRelative(BlockFace.EAST), distance);
        breakLeavesAdjacentes(tree, block.getRelative(BlockFace.WEST), distance);
    }

    private void breakLeavesAdjacentes(Tree tree, Block block, int distance) {
        if (block.getBlockData().getAsString().startsWith(tree.getLeavesData()) && Integer.valueOf(block.getBlockData().getAsString().split(",")[0].split("=")[1]) >= distance) {
            block.breakNaturally();
            breakLeavesPropagation(tree, block, distance + 1);

        } else if (block.getBlockData().getAsString().startsWith(tree.getLogDataWithOutAxis()) && !block.getBlockData().getAsString().startsWith(tree.getLogData())) {
            block.breakNaturally();
            breakLeavesPropagation(tree, block, 0);
        }
    }

    private enum Tree {
        OAK("oak"),
        BIRCH("birch"),
        SPRUCE("spruce"),
        ACACIA("acacia"),
        JUNGLE("jungle"),
        DARK_OAK("dark_oak");

        private String name;

        Tree(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public String getLogData() {
            return "minecraft:" + name + "_log[axis=y]";
        }

        public String getLogDataWithOutAxis() {
            return "minecraft:" + name + "_log";
        }

        public String getLeavesData() {
            return "minecraft:" + name + "_leaves";
        }
    }
}
