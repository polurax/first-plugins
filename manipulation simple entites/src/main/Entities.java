package main;

import org.bukkit.Effect;
import org.bukkit.EntityEffect;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import java.io.File;

public class Entities implements Listener {

    @EventHandler
    public void onDamage(EntityDamageByEntityEvent event) {
        if(event.getDamager().getType() == EntityType.PLAYER){
            Player player = (Player)event.getDamager();
            if (event.getEntity().getType() == EntityType.CHICKEN) {
                Chicken entity = (Chicken)event.getEntity();
                event.setCancelled(true);
                player.getWorld().spawnParticle(Particle.END_ROD, entity.getLocation(), 1);
                teleport(entity, player);
                player.getWorld().playSound(entity.getLocation(),"non",100,1);
                File son = new File("/non");
                if(son.exists()){
                    System.out.println("son - ok");
                }

            }
            if (event.getEntity().getType() == EntityType.RABBIT) {
                Rabbit entity = (Rabbit)event.getEntity();
                event.setCancelled(true);
                Location location = entity.getLocation().clone();
                location.setY(location.getY()+0.5);
                player.getWorld().spawnEntity(location, EntityType.RABBIT);
            }
        }

    }

    private void teleport(Entity entity, Entity player) {
        Location location = new Location(player.getWorld(), player.getLocation().getX(), player.getLocation().getY(), player.getLocation().getZ(), player.getLocation().getYaw(), player.getLocation().getPitch());
        double angle = (-location.getYaw()) / 360 * 2 * Math.PI;
        location.setZ(location.getZ() - Math.cos(angle));
        location.setX(location.getX() - Math.sin(angle));
        entity.teleport(location);
    }
}
